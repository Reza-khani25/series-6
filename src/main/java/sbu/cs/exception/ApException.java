package sbu.cs.exception;

import java.lang.RuntimeException ;

public class ApException extends RuntimeException{

}

class UnrecognizedCommandException extends ApException {

}

class NotImplementedCommandException extends ApException {

}

class BadInputException extends ApException{

}