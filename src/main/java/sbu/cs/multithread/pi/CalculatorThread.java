package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.Callable;

public class CalculatorThread implements Callable<BigDecimal> {

    private static BigDecimal MINUS_ONE = new BigDecimal(-1) ;
    private static BigDecimal MINUS_TWO = new BigDecimal(-2) ;
    private static BigDecimal ONE  = new BigDecimal(1) ;
    private static BigDecimal FOUR = new BigDecimal(4) ;
    int k ;

    public CalculatorThread(int k)
    {
        this.k = k;
    }



    public BigDecimal call(){


        BigDecimal frac1 = FOUR.divide(new BigDecimal(8 * k + 1) , 1000 , RoundingMode.HALF_EVEN) ;
        BigDecimal frac2 = MINUS_TWO.divide(new BigDecimal(8 * k + 4) , 1000 , RoundingMode.HALF_EVEN);
        BigDecimal frac3 = MINUS_ONE.divide(new BigDecimal(8 * k + 5) , 1000 , RoundingMode.HALF_EVEN);
        BigDecimal frac4 = MINUS_ONE.divide(new BigDecimal(8 * k + 6) , 1000 , RoundingMode.HALF_EVEN);

        BigDecimal sum = frac1.add(frac2).add(frac3).add(frac4) ;

        BigDecimal divisor = new BigDecimal(16).pow(k) ;
        BigDecimal frac5 = ONE.divide(divisor , 1000 , RoundingMode.HALF_EVEN) ;

        return frac5.multiply(sum) ;

    }

}
