package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

public class PICalculator {

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    public String calculate(int floatingPoint) {

        BigDecimal pi = new BigDecimal(0) ;

        ExecutorService executor = Executors.newFixedThreadPool(7) ;

        Set<Future<BigDecimal>> set = new HashSet<>() ;

        for(int i =0 ; i<1000 ; i++)
        {
            CalculatorThread c = new CalculatorThread(i) ;
            Future<BigDecimal> future = executor.submit(c) ;
            set.add(future) ;
        }

        for(Future<BigDecimal> f : set)
        {
            try {
                pi = pi.add(f.get()) ;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        pi = pi.setScale(floatingPoint , RoundingMode.FLOOR) ;

        executor.shutdown();

        return pi.toString() ;
    }



}
