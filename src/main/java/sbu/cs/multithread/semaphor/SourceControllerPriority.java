package sbu.cs.multithread.semaphor;

import java.util.concurrent.*;

public class SourceControllerPriority {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Semaphore semaphore = new Semaphore(2) ;

        Chef chef1 = new Chef("chef1" , semaphore);
        Chef chef2 = new Chef("chef2" , semaphore);
        Chef chef3 = new Chef("chef3" , semaphore);
        Chef chef4 = new Chef("chef4" , semaphore);
        Chef chef5 = new Chef("chef5" , semaphore);


       chef1.start(); chef3.start(); chef5.start();

       chef1.join(); chef3.join(); chef5.join();

       chef2.start() ; chef2.join();

       chef4.start();

//        chef1.setPriority(1);
//        chef2.setPriority(2);
//        chef3.setPriority(1);
//        chef4.setPriority(3);
//        chef5.setPriority(1);


    }
}
