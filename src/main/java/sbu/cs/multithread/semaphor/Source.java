package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class Source {

    public static void getSource() {
        // some process that is limited to 2 persons concurrently
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
