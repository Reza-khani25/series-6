package sbu.cs.multithread.semaphor;
import java.util.concurrent.Semaphore;

public class Chef extends Thread {

    Semaphore semaphore ;
    String chefName;

    public Chef(String name , Semaphore sem)
     {
        super(name);
        chefName = name ;
        this.semaphore = sem;
     }

    @Override
    public void run() {



        for (int i = 0; i < 10; i++) {

            try {
                semaphore.acquire(); //lock ( two locks are permitted)
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            /*******TASK*******/
            Source.getSource();

            semaphore.release();   //unLock

            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


        System.out.println( chefName + " done");
    }
}
