package sbu.cs.multithread.semaphor;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;


public class SourceController {

    public static void main(String[] args) {

        List<Chef> chefs = new ArrayList<>();
        Semaphore semaphore = new Semaphore(2);

        for(int i = 1 ; i<=5 ; i++)
            chefs.add(new Chef("chef"+i , semaphore));

        // all chefs have the same priority

        for(Chef c : chefs)
            c.start();
    }
}
