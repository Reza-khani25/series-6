package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class BlackThread extends ColorThread {

    CountDownLatch cdl ;

    public BlackThread(CountDownLatch c)
    {
        this.cdl = c ;
    }

    private static final String MESSAGE = "hi blues, hi whites";

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {

      //  System.out.println("black");
        printMessage();
        cdl.countDown();
    }
}
