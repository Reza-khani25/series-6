package sbu.cs.multithread.priority;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class Runner {

    public static List<Message> messages = new ArrayList<>();

    ArrayList<WhiteThread> whiteThreads = new ArrayList<>() ;
    boolean allStarts = false ;

    /**
     * add your codes to this function. this function is the caller function which will be called first.
     * changing other codes in this function is allowed.
     *
     * @param blackCount    number of black threads
     * @param blueCount     number of blue threads
     * @param whiteCount    number of white threads
     */
      public void run(int blackCount, int blueCount, int whiteCount) throws InterruptedException {


       // List<ColorThread> colorThreads = new ArrayList<>();


        CountDownLatch black_cdl = new CountDownLatch(blackCount) ;
        CountDownLatch blue_cdl = new CountDownLatch(blueCount) ;

        for (int i = 0; i < blackCount; i++) {
            BlackThread blackThread = new BlackThread(black_cdl);
            //colorThreads.add(blackThread);
            blackThread.start();
        }
        for (int i = 0; i < blueCount; i++) {
            BlueThread blueThread = new BlueThread(black_cdl , blue_cdl);
          //  colorThreads.add(blueThread);
            blueThread.start();
        }
        for (int i = 0; i < whiteCount ; i++) {
            WhiteThread whiteThread = new WhiteThread(blue_cdl );
          //  colorThreads.add(whiteThread);
            whiteThreads.add(whiteThread) ;
            whiteThread.start();
        }

        allStarts = true ;
    }

    synchronized public static void addToList(Message message) {
        messages.add(message);
    }

     public List<Message> getMessages() {

         while(!allStarts)
         {
             try {
                 Thread.sleep(10);
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }
         }

        for (WhiteThread w : whiteThreads)
        {
            try {
                w.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

         return messages;
    }


    /**
     *
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {

        new Runner().run(13 , 8 , 5);

        Thread.sleep(2000);
        System.out.println(messages.size());
    }
}
