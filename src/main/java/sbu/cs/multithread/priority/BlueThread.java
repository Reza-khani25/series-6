package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class BlueThread extends ColorThread {

    CountDownLatch black_cdl , blue_cdl;

    public BlueThread(CountDownLatch black_cdl , CountDownLatch blue_cdl)
    {
        this.black_cdl = black_cdl;
        this.blue_cdl = blue_cdl ;
    }
    private static final String MESSAGE = "hi back blacks, hi whites";

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }


    @Override
    public void run() {

        try {
            black_cdl.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //System.out.println("blue");
        printMessage();

        blue_cdl.countDown();
    }
}
